<div class="modal fade" tabindex="-1" role="dialog" id="modal-create-partial-token-survey">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo $lang["Select the question to send to an helper"]; ?></h4>
      </div>
      <div class="modal-body container-fluid">
        <?php echo CHtml::beginForm($addUser['action'], 'post', array('class' => 'form-horizontal')); ?>
            <div class="ajax-result"></div>
            <fieldset class="checkbox-list">
                <legend><?php echo $lang["Select the question to send to an helper"]; ?></legend>
                <?php foreach ($aGroups as $group) {
                    $gid = $group['id'];
                    ?>
                    <fieldset>
                        <legend><?php echo viewHelper::flatEllipsizeText($group['title'], 1); ?></legend>
                            <div class="checkbox-item question-item">
                                <input name="groups[]" id="groups-<?php echo $gid; ?>" value="<?php echo $gid; ?>" type="checkbox" onclick="">
                                <label for="groups-<?php echo $gid; ?>" class="checkbox-label control-label"><strong><?php echo $lang['All question in this group']; ?></strong></label>
                            </div>
                        <ul data-related="groups-<?php echo $gid; ?>" class="checkbox-list">
                        <?php foreach ($group['questions'] as $question) {
                            $qid = $question['id'];
                            ?>
                            <li class="checkbox-item question-item">
                                <input name="questions[]" id="questions-<?php echo $qid; ?>" value="<?php echo $qid; ?>" type="checkbox"  onclick="">
                                <label for="questions-<?php echo $qid; ?>" class="checkbox-label control-label"><?php echo viewHelper::flatEllipsizeText($question['title'], 1, 160); ?></label>
                            </li>
                        <?php } ?>
                    </fieldset>
                <?php } ?>
            </fieldset>
            <fieldset>
                <legend><?php echo $lang["Email and message"]; ?></legend>
                <ul class='list-unstyled'>
                    <li class="form-group"><?php
                        echo CHtml::label($lang["Your contact information (Last name and first name)"], "byname", array('class' => "col-sm-4 control-label"));
                        echo CHtml::tag(
                            "div",
                            array('class' => "col-sm-7"),
                            CHtml::textField("byname", $addUser['byname'], array('class' => 'form-control'))
                        );
                        ?><li>
                    <li class="form-group"><?php
                        echo CHtml::label($lang["Your email"], "bymail", array('class' => "col-sm-4 control-label"));
                        echo CHtml::tag(
                            "div",
                            array('class' => "col-sm-7"),
                            CHtml::textField("bymail", $addUser['bymail'], array('class' => 'form-control'))
                        );
                        ?><li>
                    <?php foreach ($addUser['email']['attributes'] as $attribute => $dataAttribute) { ?>
                        <li class="form-group"><?php
                            echo CHtml::label($dataAttribute['label'], $dataAttribute['id'], array('class' => "col-sm-4 control-label"));
                            $field = $dataAttribute['type'] . 'Field';
                            echo CHtml::tag(
                                "div",
                                array('class' => "col-sm-7"),
                                CHtml::$field($dataAttribute['name'], "", array('id' => $dataAttribute['id'],'class' => 'form-control','data-default' => "",'required' => $dataAttribute['required']))
                            );
                                                ?></li>
                    <?php } ?>
                    <li class="form-group">
                        <?php
                            $textArea = Chtml::textArea('emailmessage', $addUser['email']['message'], array('rows' => 20,'class' => 'form-control','height' => "12em",'data-default' => $addUser['email']['message']));
                            $textArea = "";
                        if ($addUser['email']['html']) {
                            $textArea = Yii::app()->getController()->widget('yiiwheels.widgets.html5editor.WhHtml5Editor', array(
                                'name' => 'message',
                                'id' => 'message',
                                'value' => $addUser['email']['message'],
                                'pluginOptions' => array(
                                    'html' => true,
                                    'font-styles' => false,
                                    'lists' => false,
                                    'image' => false,
                                    'link' => false,
                                    'useLineBreaks' => false,
                                    'autoLink' => true,
                                    'parserRules' => 'js:wysihtml5ParserRules'
                                ),
                                'htmlOptions' => array(
                                    'class ' => 'form-control',
                                    'data-default' => "",
                                    'height' => "12em",
                                ),
                            ), true);
                        }
                            echo CHtml::label(gT("Message"), 'message', array('class' => "col-sm-4 control-label")) .
                            CHtml::tag(
                                "div",
                                array('class' => "col-sm-7"),
                                $textArea . "<p class='help-block'>" . $addUser['email']['helpMessage'] . "</p>"
                            );
                            ?>
                        
                    </li>

                </ul>
            </fieldset>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8 text-right">
                    <?php echo CHtml::htmlButton("Create and send", array('type' => 'submit','class' => "btn btn-primary")); ?>
                </div>
            </div>
        <?php echo CHtml::endForm(); ?>
        </div>
        <?php
          //echo CHtml::htmlButton($lang['Close'],array('type'=>'button','class'=>"btn btn-warning",'data-dismiss'=>"modal"));
        ?>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
