<?php

/**
 * Plugin helper
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @since 2.0.0-alpha0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace partialTokenSurveyFill;

use Yii;
use App;
use Question;
use QuestionGroupL10n;
use QuestionL10n;

class Utilities
{
    /**
     * @param integer $surveyId
     * @param string $language :
     * @return array : groups with question isinde for view
     */
    public static function getAllGroupsWithQuestions($surveyId, $language)
    {
        Yii::import('application.helpers.viewHelper');
        $questionTable = Question::model()->tableName();
        $command = App()->db->createCommand()
            ->select("qid,{{questions}}.gid as gid, {{groups}}.group_order, {{questions}}.question_order")
            ->from($questionTable)
            ->where("({{questions}}.sid = :sid AND {{questions}}.parent_qid = 0)")
            ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid")
            ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
            ->bindParam(":sid", $surveyId, \PDO::PARAM_INT);
        $allQuestions = $command->query()->readAll();
        $aGroups = array();
        $gid = 0;
        foreach ($allQuestions as $question) {
            $qid = $question['qid'];
            if ($gid != $question['gid']) {
                if ($gid) {
                    $aGroups[$gid]['questions'] = $aQuestions;
                }
                $gid = $question['gid'];
                $oGroupL10n = QuestionGroupL10n::model()->find("gid = :gid and language = :language", array(":gid" => $gid,":language" => $language));
                $aGroups[$gid] = array(
                    'id' => $gid,
                    'title' => \viewHelper::purified($oGroupL10n->group_name),
                    'questions' => array(),
                );
                $aQuestions = array();
            }
            $gid = $question['gid'];
            $oQuestionL10n = QuestionL10n::model()->find("qid = :qid and language = :language", array(":qid" => $qid,":language" => $language));
            $aQuestions[] = array(
                'id' => $qid,
                'title' => \viewHelper::purified($oQuestionL10n->question),
            );
        }
        /* Last group not closed */
        $aGroups[$gid]['questions'] = $aQuestions;
        return $aGroups;
    }

    /**
     * Get the final filter using question and group to shown
     * @param integer surveyid
     * @param integer[] list of group id
     * @param integer[] list of question id
     * @return array
     */
    public static function getFinalFilter($surveyId, $aGroupId = array(), $aQuestionId = array())
    {
        $aQuestionId = (array) $aQuestionId;
        $aGroupId = (array) $aGroupId;

        $questionTable = Question::model()->tableName();
        $command = Yii::app()->db->createCommand()
            ->select("qid,{{questions}}.gid as gid,{{groups}}.group_order, {{questions}}.question_order")
            ->from($questionTable)
            ->where("({{questions}}.sid = :sid AND {{questions}}.parent_qid = 0)")
            ->join('{{groups}}', "{{groups}}.gid = {{questions}}.gid")
            ->order("{{groups}}.group_order asc, {{questions}}.question_order asc")
            ->bindParam(":sid", $surveyId, \PDO::PARAM_INT);
        $allQuestions = $command->query()->readAll();
        $finalFilter = array();
        $checked = "";
        foreach ($allQuestions as $question) {
            if (!in_array($question['qid'], $aQuestionId) && !in_array($question['gid'], $aGroupId)) {
                $finalFilter[] = $question['qid'];
            }
        }
        return $finalFilter;
    }
}
