<?php

/**
 * This file is part of reloadAnyResponse plugin
 *
 * Model for partialTokenSurveyFill : tokenFilter
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @since 1.0.0
 */

namespace partialTokenSurveyFill\models;

use Yii;
use LSActiveRecord;

class TokenFilter extends LSActiveRecord
{
    /**
     * @property integer $sid survey
     * @property integer $srid : response id
     * @property string $token : token
     * @property string $filter (json string)
     * @property string $sourcetoken : token that create the token
    */

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    /** @inheritdoc */
    public function tableName()
    {
        // todo : find a way to use API
        return '{{partialtokensurveyfill_tokenFilter}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return array('sid', 'token');
    }

    public function rules()
    {
        return array(
            array('sid', 'numerical', 'integerOnly' => true),
            array('srid', 'numerical', 'integerOnly' => true),
            array('srid', 'required'),
            array('token', 'length', 'min' => 1, 'max' => 50),
            array('sourcetoken', 'length', 'min' => 1, 'max' => 50),
        );
    }

    public function setFilters($filters)
    {
        $this->filter = json_encode($filters);
    }

    public function getFilter()
    {
        return json_decode($this->filter);
    }

    /**
     * Get filters
     * @return null|array
     */
    public function getSgqaFilters()
    {
        if (empty($this)) {
            return null;
        }
        $filters = json_decode($this->filter);
        if (empty($filters)) {
            return array();
        }
        if (!is_array($filters)) {
            return array();
        }
        $sgqaFilters = array();
        foreach ($filters as $qid) {
            $oQuestion = \Question::model()->find(
                'qid = :qid and sid = :sid',
                array(':qid' => $qid,':sid' => $this->sid)
            );
            if ($oQuestion && !$oQuestion->parent_qid) {
                $sgqaFilters = array_merge(
                    $sgqaFilters,
                    \getQuestionInformation\helpers\surveyCodeHelper::getQuestionColumn($qid)
                );
            } else {
                \Yii::log(
                    "Invalid question id $qid for {$this->token} in {$this->sid}",
                    'warning',
                    'plugin.partialTokenSurveyFill.models.tokenFilter.getSgqaFilters'
                );
            }
        }
        return $sgqaFilters;
    }
}
