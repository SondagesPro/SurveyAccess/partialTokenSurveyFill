/* html(parser rules */
var wysihtml5ParserRules = {
  tags: {
    strong: {},
    b: {
      rename_tag: "strong"
    },
    em: {},
    i: {
      rename_tag: "em"
    },
    br: {},
    p: {},
    div: {},
    span: {},
    ul: {},
    ol: {},
    li: {},
    a: {
      set_attributes: {
        target: "_blank"
      },
      check_attributes: {
        href: "url"
      }
    },
    img: {
      check_attributes: {
        src: "url",
        width: "numbers",
        alt: "alt",
        height: "numbers"
      },
    },
    link: {
      remove: 1
    },
    script: {
      remove: 1
    }
  }
};
