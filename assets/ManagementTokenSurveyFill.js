var partialTokenSurveyFill = {
    init: function (options) {
        if (!$("[data='showtokenfillmanagement']").length) {
            $("#navigator-container").append("<div class='col-xs-12'><button type='button' data='showtokenfillmanagement' class='btn btn-default'>" + options.lang.buttonAction + "</button></div>");
        }
        $(".tokenfillmanagement-button-wrapper").append("<div class='col-xs-12'><button type='button' data='showtokenfillmanagement' class='btn btn-default'>" + options.lang.buttonAction + "</button></div>");

        $("[data='showtokenfillmanagement']").on("click", partialTokenSurveyFill.openTokenFillManagement);
        $("#modal-create-partial-token-survey button:submit").on("click", partialTokenSurveyFill.createTokenFilterSurvey);
        $("#modal-create-partial-token-survey").on("shown.bs.modal", function (e) {
            if (e.target) {
                partialTokenSurveyFill.updateHeightModalbody(e.target);
            }
        });
        $("#modal-create-partial-token-survey [id^='groups-']").on("change", function () {
            if ($(this).is(":checked")) {
                $("[data-related=" + $(this).attr("id") + "]").slideUp();
            } else {
                $("[data-related=" + $(this).attr("id") + "]").slideDown();
            }
        });
        /* since emailmessage is not in body when loading, muts launch it manually */
        jQuery('#emailmessage').wysihtml5({ 'html': true, 'font-styles': false, 'lists': true, 'image': false, 'link': false, 'useLineBreaks': false, 'autoLink': true, 'parserRules': wysihtml5ParserRules, 'stylesheets': [] });
        /* some fix */
        $(".wysihtml5-toolbar .btn").each(function () { /* bad hack */
            $(this).addClass("btn-default btn-xs");
        });
        $(".wysihtml5-toolbar .icon-pencil").addClass("fa fa-edit");
        $(".wysihtml5-toolbar .icon-list").addClass("fa fa-list");
        $(".wysihtml5-toolbar .icon-list").addClass("fa fa-list-ul");
        $(".wysihtml5-toolbar .icon-th-list").addClass("fa fa-list-ol");
        $(".wysihtml5-toolbar .icon-indent-right").addClass("fa fa-outdent");
        $(".wysihtml5-toolbar .icon-indent-left").addClass("fa fa-indent");
    },
    openTokenFillManagement: function (event) {
        event.preventDefault();
        $("#modal-create-partial-token-survey").modal('show');
    },
    createTokenFilterSurvey: function (event, data) {
        //~ event.preventDefault();
        data = $.extend({ source: null }, data);
        if (data.source == 'control') {
            return;
        }
        event.preventDefault();
        var $form = $("#modal-create-partial-token-survey form")[0];
        if (!$form.checkValidity()) {
            $(event.target).trigger('click', { source: 'control' });
            return;
        }
        var url = $("#modal-create-partial-token-survey form").attr("action");
        var params = $("#modal-create-partial-token-survey form").serializeArray();
        var htmlAlert = "";
        $("#modal-create-partial-token-survey .ajax-result").html("");
        $.ajax({
            url: url,
            type: 'POST',
            data: params,
            dataType: 'json'
        })
            .done(function (data) {
                data = $.extend({}, { status: 'warning' }, data);
                if (data.status == "success" || data.status == "info") {
                    htmlAlert = "<div class='alert alert-" + data.status + "'>" + data.html + "</div>";
                    $("#modal-create-partial-token-survey form").find("input[type='email'],input:text,textarea").each(function () {
                        $(this).val("");
                        if ($(this).data("default")) {
                            $(this).val($(this).data("default"));
                        }
                    });
                    $("#modal-create-partial-token-survey .ajax-result").html(htmlAlert);
                    $("#modal-create-partial-token-survey .modal-body").animate({
                        scrollTop: $("#modal-create-partial-token-survey .modal-body .ajax-result").position().top
                    }, 500);
                    //~ if($("#emailbody").data("wysihtml5")) {
                    //~ $("#emailbody").data("wysihtml5").editor.setValue($("#emailbody").data("default"));
                    //~ }
                    //~ if($("#emailmessage").data("wysihtml5")) {
                    //~ $("#emailmessage").data("wysihtml5").editor.setValue($("#emailmessage").data("default"));
                    //~ }
                    return;
                }
                if (data.html) {
                    var className = data.status;
                    if (data.status == 'error') {
                        className = 'danger';
                    }
                    htmlAlert = "<div class='alert alert-" + className + "'>" + data.html + "</div>";
                    $("#modal-create-partial-token-survey .ajax-result").html(htmlAlert);
                    $("#modal-create-partial-token-survey .modal-body").animate({
                        scrollTop: $("#modal-create-partial-token-survey .modal-body .ajax-result").position().top
                    }, 500);
                    return;
                }
            })
            .fail(function (jqXHR, textStatus) {
                $("#modal-create-partial-token-survey .ajax-result").html("<div class='alert alert-danger'>" + jqXHR.responseText + "</div>");
                $("#modal-create-partial-token-survey .modal-body").animate({
                    scrollTop: $("#modal-create-partial-token-survey .modal-body .ajax-result").position().top
                }, 500);
            });
        return;
    },
    updateHeightModalbody: function (modal) {
        var navbarFixed = 0;
        if (false && (".navbar-fixed-top").length) {
            navbarFixed = $(".navbar-fixed-top").outerHeight();
        }

        var modalHeader = 0;
        if ($(modal).find(".modal-header").length) {
            modalHeader = $(modal).find(".modal-header").outerHeight();
        }
        var modalFooter = 0;
        if ($(modal).find(".modal-footer").length) {
            modalFooter = $(modal).find(".modal-footer").outerHeight();
        }
        var finalHeight = Math.max(400, $(window).height() - (navbarFixed + modalHeader + modalFooter + 38));// Not less than 150px
        $(modal).find(".modal-lg").css("margin-top", navbarFixed + 4).css("margin-bottom", 4);
        $(modal).find(".modal-body").height(finalHeight);
        //$(modal).find(".modal-body > iframe").css("height",finalHeight);
    }
};
