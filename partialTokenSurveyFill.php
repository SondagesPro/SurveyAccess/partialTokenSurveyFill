<?php

/**
 * Partially fill a survey by another participant
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2018-2024 Denis Chenu <www.sondages.pro>
 * @copyright 2018-2024 OECD <www.oecd.org>

 * @license AGPL
 * @version 2.0.0-alpha1
 *
 */
class partialTokenSurveyFill extends PluginBase
{
    protected static $name = 'partialTokenSurveyFill';
    protected static $description = '';

    protected $storage = 'DbStorage';

    protected static $dbVersion = 3;

    /**
    * Add function when you want
    */
    public function init()
    {
        /* Basic */
        $this->subscribe("beforeActivate");
        $this->subscribe("afterPluginLoad");

        /* Settings */
        $this->subscribe("beforeSurveySettings");
        $this->subscribe("newSurveySettings");

        /* To be done before access */
        $this->subscribe("beforeSurveyPage"); /* Need to create response + set relevance + start survey … */
        $this->subscribe("getPluginTwigPath", "registerScriptInTwig"); /* To see where am i, debug part */

        /* create token and send email */
        $this->subscribe("newDirectRequest");

        /* When survey is submitted : disable current token and update original response */
        $this->subscribe("afterSurveyComplete");

        /* alias for extra class */
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
    }

    public function registerScriptInTwig()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        /* Todo : check controller , needed only for survey controller */
        $surveyId = intval(Yii::app()->getRequest()->getParam("sid"));
        $token = Yii::app()->getRequest()->getParam("token");
        if (empty($surveyId) || empty($token)) {
            $this->unsubscribe("getPluginTwigPath");
            return;
        }
        if (!$this->isPartialTokenFillActive($surveyId)) {
            $this->unsubscribe("getPluginTwigPath");
            return;
        }
        $dialogManagementTokenSurveyFill = $this->getManagementTokenSurveyFill($surveyId, $token);
        if (!empty($dialogManagementTokenSurveyFill)) {
            App()->getClientScript()->registerCssFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/ManagementTokenSurveyFill.css'));
            App()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/ManagementTokenSurveyFill.js'));
            App()->getClientScript()->registerScriptFile(Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/wysihtml5ParserRules.js'));
            $aOptions = array(
                'lang' => array(
                    'buttonAction' => $this->gT("Add a participant to help on filling survey"),
                ),
            );
            $ManagementTokenSurveyFill = "$('body').prepend(" . json_encode($dialogManagementTokenSurveyFill) . ");\npartialTokenSurveyFill.init(" . json_encode($aOptions) . ");\n";
            Yii::app()->getClientScript()->registerScript("ManagementTokenSurveyFill", $ManagementTokenSurveyFill, CClientScript::POS_READY);
        }
        $this->unsubscribe("getPluginTwigPath");
    }

    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $haveGetQuestionInformation = Yii::getPathOfAlias('getQuestionInformation');
        if (!$haveGetQuestionInformation) {
            $this->getEvent()->set('message', $this->gT("You need getQuestionInformation plugin"));
            $this->getEvent()->set('success', false);
            return;
        }
        $this->setPartialTokenFillDb();
    }
    public function afterPluginLoad()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $this->setPartialTokenFillDb();
    }
    /** @inheritdoc **/
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get("survey");
        $oSurvey = Survey::model()->findByPk($surveyId);
        $bShowRealOptionValues = $oSurvey->bShowRealOptionValues;
        if (!$bShowRealOptionValues) {
            $oSurvey->bShowRealOptionValues = true;
            $oSurvey->setOptions($oSurvey->gsid);
        }

        $contentText = "";
        if ($oSurvey->getHasTokensTable()) {
            if (!$oSurvey->getIsAnonymized()) {
                $contentText .= CHtml::tag("p", array('class' => 'alert alert-success'), $this->gT("This survey has a participants table and is not anonymous: partialTokenSurveyFill can be used."));
                /* Add alert about  Enable participant-based response persistence: */
                if (!$oSurvey->getIsTokenAnswersPersistence()) {
                    $contentText .= CHtml::tag("p", array('class' => 'alert alert-warning'), $this->gT("Token answers persistence is set to off, participant can not reload answer."));
                }
            } else {
                $contentText .= CHtml::tag("p", array('class' => 'alert alert-warning'), $this->gT("This survey has a participants table but is anonymous: partialTokenSurveyFill cannot be used."));
            }
        } else {
            $contentText .= CHtml::tag("p", array('class' => 'alert alert-warning'), $this->gT("This survey doesn't have a participants table: partialTokenSurveyFill cannot be used."));
        }
        $this->getEvent()->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
                'settings' => array(
                    'information' => array(
                        'type' => 'info',
                        'content' => $contentText,
                    ),
                    'active' => array(
                        'type' => 'boolean',
                        'label' => $this->gT("Activate."),
                        'current' => $this->get('active', 'Survey', $surveyId, false)
                    ),
                    'allowAll' => array(
                        'type' => 'boolean',
                        'label' => $this->gT("Allow all question."),
                        'current' => $this->get('allowAll', 'Survey', $surveyId, true),
                        'help' => $this->gT("No filter is needed, allow to select all questions to be shown. This was checked only when add an user."),
                    ),
                    'tokenattribute' => array(
                        'type' => 'select',
                        'options' => $this->getPartialTokenFillTokensAttributeList($surveyId),
                        'htmlOptions' => array(
                            'empty' => $this->gT("No attribute control"),
                        ),
                        'label' => $this->gT("Participant allowed to ask another user help"),
                        'help' => $this->gT("Participant with existing filter can never ask to someone else."),
                        'current' => $this->get('tokenattribute', 'Survey', $surveyId, ""),
                    ),
                    'emailTemplateInvite' => array(
                        'type' => 'select',
                        'label' => $this->gT("Mail template to use for invitation"),
                        'options' => array(
                            "invite" => gT("Invitation"),
                            "remind" => gT("Reminder"),
                            "register" => gT("Registration"),
                            "admin_notification" => gT("Admin notification"),
                            "admin_responses" => gT("Admin detailed response"),
                        ),
                        'help' => $this->gT("In mail template {SURVEYDESCRITION} and {MESSAGE} was replaced by the message sent by user."),
                        'current' => $this->get('emailTemplateInvite', 'Survey', $surveyId, "invite"),
                    ),
                    'emailShowFirstname' => array(
                        'type' => 'boolean',
                        'label' => $this->gT("Shown first name when adding an helper"),
                        'current' => $this->get('emailShowFirstname', 'Survey', $surveyId, 0),
                    ),
                    'emailShowLastname' => array(
                        'type' => 'boolean',
                        'label' => $this->gT("Shown last name when adding an helper"),
                        'current' => $this->get('emailShowLastname', 'Survey', $surveyId, 1),
                    ),
                    'emailTemplateConfirm' => array(
                        'type' => 'select',
                        'label' => $this->gT("[WIP] Mail template to use for confirmation"),
                        'htmlOptions' => array(
                            'empty' => $this->gT("None, don't send confirmation"),
                        ),
                        'options' => array(
                            "invite" => gT("Invitation"),
                            "remind" => gT("Reminder"),
                            "register" => gT("Registration"),
                            "confirm" => gT("Confirmation"),
                            "admin_notification" => gT("Admin notification"),
                            "admin_responses" => gT("Admin detailed response"),
                        ),
                        //'help' => $this->gT("In mail template {SURVEYDESCRITION} and {MESSAGE} was replaced by our automatic message."),
                        'current' => $this->get('emailTemplateConfirm', 'Survey', $surveyId, null),
                    ),
                ),
        ));

        /* Reset option for other plugin */
        if (!$bShowRealOptionValues) {
            $oSurvey->bShowRealOptionValues = false;
            $oSurvey->setOptions($oSurvey->gsid);
        }
    }

    /** @inheritdoc **/
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (Yii::app()->getConfig('previewmode')) {
            return;
        }
        $surveyId = $this->getEvent()->get("surveyId");
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$this->isPartialTokenFillActive($surveyId)) {
            return;
        }
        $this->subscribe('setVariableExpressionEnd', 'setPartialTokenFillFilterFor');
        $token = App()->getRequest()->getParam('token');
        if ($this->isPartialTokenFillTokenValid($surveyId, $token)) {
            $filters = $this->getPartialTokenFillFilterFor($surveyId, $token, true);
            $noUpadteAttributes = array('id', 'token', 'submitdate', 'lastpage', 'datestamp', 'refurl', 'seed', 'startlanguage');
            $this->createPartialTokenFillResponseForToken($surveyId, $token);
        }
    }

    /**
     * Create response like needed
     * Do it before reload response
     **/
    private function createPartialTokenFillResponseForToken($surveyId, $token)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);

        $oResponse = Response::model($surveyId)->findByAttributes(array('token' => $token));
        if (!empty($oResponse)) {
            /* Reponse already exist : no update */
            return;
        }
        $oResponseFilter = \partialTokenSurveyFill\models\TokenFilter::model()->findByPk(['sid' => $surveyId, 'token' => $token]);
        if (empty($oResponseFilter)) {
            /* Must not happen */
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $language = Yii::app()->getRequest()->getParam("lang");
        if (!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $oSourceResponse = Response::model($surveyId)->findByPk($oResponseFilter->srid);
        $oResponse = Response::create($surveyId);
        $oResponse->token = $token;
        $oResponse->startlanguage = $language;
        if ($oSurvey->getIsDateStamp()) {
            $oResponse->startdate = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
            $oResponse->datestamp = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
        }
        $oResponse->save();
        if (!empty($oSourceResponse)) {
            $aAttributes = $oSourceResponse->getAttributes();
            $resetAttributes = array('id','token','submitdate','lastpage','datestamp','refurl'); // Keep same seed ?
            foreach ($resetAttributes as $attribute) {
                unset($aAttributes[$attribute]);
            }
            $oResponse->saveAttributes($aAttributes);
        } else {
            $this->log("New filtered response set without original response", 'warning');
        }
    }

    /** See event **/
    public function setPartialTokenFillFilterFor()
    {
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $knownVars = $event->get('knownVars');
        if (empty($surveyId) || empty($knownVars['TOKEN']['code'])) {
            return;
        }
        $token = $knownVars['TOKEN']['code'];
        $aQidFilteredForThisToken = $this->getPartialTokenFillFilterFor($surveyId, $token);
        if (empty($aQidFilteredForThisToken)) {
            return;
        }
        tracevar($aQidFilteredForThisToken);
        foreach ($knownVars as $sgqa => $knownVar) {
            if (!empty($knownVar['qid']) && in_array($knownVar['qid'], $aQidFilteredForThisToken)) {
                $knownVars[$sgqa]['hidden'] = true;
            }
        }
        $event->set('knownVars', $knownVars);
        $questionSeq2relevances = $event->get('questionSeq2relevance');
        foreach ($questionSeq2relevances as $key => $questionSeq2relevance) {
            if (!empty($questionSeq2relevance['qid']) && in_array($questionSeq2relevance['qid'], $aQidFilteredForThisToken)) {
                $questionSeq2relevances[$key]['hidden'] = true;
            }
        }
        $event->set('questionSeq2relevance', $questionSeq2relevances);
    }

    /**
     * Update original reponse if needed
     */
    public function afterSurveyComplete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get("surveyId");
        if (!$this->isPartialTokenFillActive($surveyId)) {
            return;
        }
        $responseId = $this->getEvent()->get("responseId");
        $response = $this->pluginManager->getAPI()->getResponse($surveyId, $responseId);
        if (empty($response['token'])) {
            return;
        }

        $oTokenFilter = \partialTokenSurveyFill\models\TokenFilter::model()->findByPk(array('sid' => $surveyId, 'token' => $response['token']));
        if (empty($oTokenFilter)) {
            $this->log("tokenFilter not found for token {$response['token']} in survey {$surveyId}", 'trace');
            return;
        }
        if (empty($oTokenFilter->srid)) {
            $this->log("tokenFilter found for token {$response['token']} in survey {$surveyId} but no related srid", 'error');
            return;
        }
        $oTokenConfirm = \Token::model($surveyId)->find("token = :token", array(':token' => $oTokenFilter->sourcetoken));
        if (empty($oTokenConfirm)) {
            $this->log("tokenFilter found for token {$response['token']} in survey {$surveyId} but invalid related token", 'error');
        }
        $oResponseToUpdate = \Response::model($surveyId)->findByPk($oTokenFilter->srid);
        if (empty($oTokenFilter->srid)) {
            $this->log("tokenFilter found for token {$response['token']} in survey {$surveyId} bit no related srid", 'error');
            $this->getEvent()->getContent($this)
                  ->addContent('<div class="alert alert-warning">' . $this->gT("We are unable to find related response to update.") . '</div>');
            return;
        }
        /* OK we have the session relevance set, then a filter must exist … */
        $filters = $this->getPartialTokenFillFilterFor($surveyId, $response['token'], true);
        $aResponse = Response::model($surveyId)->findByPk($response['id'])->getAttributes();
        $resetAttributes = array('id','token','submitdate','lastpage','datestamp','refurl','seed','startlanguage');
        $resetAttributes = array_merge($resetAttributes, array_keys($filters));
        foreach ($resetAttributes as $attribute) {
            unset($aResponse[$attribute]);
        }
        $oResponseToUpdate->saveAttributes($aResponse);

        /* Delete reponse */
        \Response::model($surveyId)->findByPk($response['id'])->delete();
        $this->getEvent()->set('responseId', null);
        unset($_SESSION['survey_' . $surveyId]['srid']);
        /* Disable token */
        $oToken = \Token::model($surveyId)->findByToken($response['token']);
        $oToken->validuntil = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i", Yii::app()->getConfig("timeadjust"));
        $oToken->save();
        if ($this->get('emailTemplateConfirm', 'Survey', $surveyId, null) && $oTokenConfirm) {
            $emailToken = new \partialTokenSurveyFill\helpers\EmailToken($surveyId, $this->get('emailTemplateConfirm', 'Survey', $surveyId));
            $emailToken->sendMail($oTokenConfirm);
        }
        $message = $this->gT("Your reponse update original response. Access with this survey with this token was disabled.");
        $this->getEvent()->getContent($this)
              ->addContent("<div class='alert alert-success'>" . $message . "</div>");
        /* @todo : Send an email to asker */
    }

    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->getEvent()->get('target') != get_class($this)) {
            return;
        }
        $surveyId = App()->getRequest()->getQuery('sid', App()->getRequest()->getQuery('surveyid'));
        if ($surveyId && App()->getRequest()->getQuery('action') == 'adduser') {
            $this->addFilteredUserForSurvey($surveyId);
            App()->end(); // Not needed but more clear
        }
    }

    /**
     * Create a token + related filter
     * Send the email
     */
    private function addFilteredUserForSurvey($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aResult = array(
            'status' => null,
        );
        if (!$oSurvey) {
            throw new CHttpException(404, 'Invalid survey id.');
        }
        if (!$this->isPartialTokenFillActive($surveyId)) {
            throw new CHttpException(403, 'This is disable for this survey.');
        }
        $currenttoken = Yii::app()->getRequest()->getParam("currenttoken");
        if (!$this->checkPartialTokenTokenHaveRight($surveyId, $currenttoken)) {
            throw new CHttpException(403, 'This is disable for this token.');
        }
        $srid = Yii::app()->getRequest()->getParam("srid");
        $oResponse = Response::model($surveyId)->findByPk($srid);
        if (empty($oResponse)) {
            $this->returnJson(array(
                'status' => 'warning',
                'html' => $this->gT("You need to save one time current reponse."),
            ));
        }
        if (empty($oResponse->token)) {
            throw new CHttpException(403, 'This is disable for this response.');
        }
        if ($oResponse->token != $currenttoken) {
            throw new CHttpException(403, 'This is disable for this response with this token.');
        }
        /* OK, we got it */
        $questionFilter = Yii::app()->getRequest()->getPost("questions");
        $groupFilter = Yii::app()->getRequest()->getPost("groups");
        if (empty($questionFilter) && empty($groupFilter)) {
            $this->returnJson(array(
                'status' => 'warning',
                'html' => $this->gT("You need to check one question minimum."),
            ));
        }

        /* filter must be inverse filter … */
        /* Create the filter */
        $finalFilter = \partialTokenSurveyFill\Utilities::getFinalFilter($surveyId, $groupFilter, $questionFilter);
        if (empty($finalFilter) && !$this->get('allowAll', 'Survey', $surveyId, true)) {
            $this->returnJson(array(
                'status' => 'warning',
                'html' => $this->gT("The question you choose seems unavailable in this survey."),
            ));
        }
        /* Create the token and his related filter */
        $emailToken = new \partialTokenSurveyFill\helpers\EmailToken($surveyId, $this->get('emailTemplateInvite', 'Survey', $surveyId, "invite"));
        $oToken = $emailToken->getNewToken();
        if ($oToken->hasErrors()) {
            $this->returnJson(array(
                'status' => 'error',
                'html' => CHtml::errorSummary($oToken),
            ));
        }
        /* Save the filter */
        $oTokenFilter = new \partialTokenSurveyFill\models\TokenFilter();
        $oTokenFilter->sid = $surveyId;
        $oTokenFilter->token = $oToken->token;
        $oTokenFilter->sourcetoken = $currenttoken;
        $oTokenFilter->srid = $srid;
        $oTokenFilter->setFilters($finalFilter);
        if (!$oTokenFilter->save()) {
            $this->returnJson(array(
                'status' => 'error',
                'html' => CHtml::errorSummary($oTokenFilter),
            ));
        }
        $currenttoken = App()->getRequest()->getParam('currenttoken');
        if ($currenttoken) {
            $oCurrentToken = Token::model($surveyId)->find("token = :token", array(":token" => $currenttoken));
            if ($oCurrentToken) {
                $aReplaceField["ADMINNAME"] = $oCurrentToken->firstname . " " . $oCurrentToken->lastname;
                $aReplaceField["ADMINEMAIL"] = $oCurrentToken->email;
                $aReplaceField["USERNAME"] = $oCurrentToken->firstname . " " . $oCurrentToken->lastname;
                $aReplaceField["USEREMAIL"] = $oCurrentToken->email;
            }
        }
        if (!empty(Yii::app()->getRequest()->getPost('byname'))) {
            $aReplaceField["ADMINNAME"] = Yii::app()->getRequest()->getPost('byname');
            $aReplaceField["USERNAME"] = Yii::app()->getRequest()->getPost('byname');
        }
        if (!empty(Yii::app()->getRequest()->getPost('bymail'))) {
            $aReplaceField["ADMINEMAIL"] = Yii::app()->getRequest()->getPost('bymail');
            $aReplaceField["USEREMAIL"] = Yii::app()->getRequest()->getPost('bymail');
        }
        /* Response was created just BEFORE load response (and surely load it …) */
        if (!$emailToken->sendMail($oToken, array('message' => App()->getRequest()->getParam('message')), $aReplaceField)) {
            $html = sprintf("Token created but unable to send the email, token code is %s", CHtml::tag('code', array(), $oToken->token));
            if (App()->getConfig("debug") > 0) {
                $html .= CHtml::tag("hr");
                $html .= CHtml::tag("strong", array('class' => 'block'), 'Error return by mailer:');
                $html .= CHtml::tag("div", array(), $emailToken->mailError);
            }
            $this->returnJson(array(
                'status' => 'warning',
                'html' => $html,
            ));
        }
        $this->returnJson(array(
            'status' => 'success',
            'html' => $this->gT("Success! The account was created and the invitation email has been sent."),
        ));
    }

    /**
     * Set the DB needed for plugin
     */
    private function setPartialTokenFillDb()
    {
        if (intval($this->get("dbVersion")) >= self::$dbVersion) {
            return;
        }
        // Specify case sensitive collations for the token
        $sCollation = '';
        if (Yii::app()->db->driverName == 'mysql' || Yii::app()->db->driverName == 'mysqli') {
            $sCollation = "COLLATE 'utf8mb4_bin'";
        }
        if (
            Yii::app()->db->driverName == 'sqlsrv'
            || Yii::app()->db->driverName == 'dblib'
            || Yii::app()->db->driverName == 'mssql'
        ) {
            $sCollation = "COLLATE SQL_Latin1_General_CP1_CS_AS";
        }
        /* Table creation : only when activate */
        if (!$this->api->tableExists($this, 'tokenFilter')) {
            $this->api->createTable($this, 'tokenFilter', array(
                'sid' => 'int NOT NULL',
                'token' => "string(50) {$sCollation} NOT NULL", // LS 3.15.0 is string(35)
                'srid' => 'int', // Get original srid : we need it to know what is the reponse to reload …
                'filter' => 'text', // Save as json
                'sourcetoken' => "string(50) {$sCollation}", // LS 3.15.0 is string(35)
            ));
            $tableName = $this->api->getTable($this, 'tokenFilter')->tableName();
            Yii::app()->getDb()->createCommand()->addPrimaryKey('tokenfilter_sidtoken', $tableName, 'sid,token');
            $this->set("dbVersion", self::$dbVersion);
            return;
        }
        /* beforeActivate event : plugin still not set, $this->get("dbVersion") is null */
        $thisPluginActive = Plugin::model()->find("name = :name and active = :active", array(":name" => "partialTokenSurveyFill",":active" => '1'));
        if (!$thisPluginActive) {
            return;
        }
        $tableName = $this->api->getTable($this, 'tokenFilter')->tableName();
        $tokenFilterTable = $this->api->getTable($this, 'tokenFilter');
        if ($this->get("dbVersion") < 2) {
            if (!in_array('sourcetoken', $tokenFilterTable->getTableSchema()->getColumnNames())) {
                Yii::app()->db->createCommand()->addColumn($tableName, 'sourcetoken', "string(50) {$sCollation}");
            }
            $this->set("dbVersion", 2);
        }
        if ($this->get("dbVersion") < 3) {
            if (!empty($tokenFilterTable->getTableSchema()->primaryKey)) {
                Yii::app()->getDb()->createCommand()->dropPrimaryKey('tokenfilter_sidtoken', $tableName);
            }
            Yii::app()->getDb()->createCommand()->addPrimaryKey('tokenfilter_sidtoken', $tableName, 'sid,token');
            $this->set("dbVersion", 3);
        }

        $this->set("dbVersion", self::$dbVersion);
    }

    /**
     * Test if this token is valid
     * @param int $surveyId
     * @param string $token
     * @return boolean
     */
    private function isPartialTokenFillTokenValid($surveyId, $token)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if ($oSurvey->alloweditaftercompletion == 'Y') {
            $oToken = Token::model($surveyId)->editable()->findByAttributes(array('token' => $token)); // Date only
        } else {
            $oToken = Token::model($surveyId)->usable()->findByAttributes(array('token' => $token)); // Date only
        }
        if (empty($oToken)) {
            return false;
        }
        return true;
    }

    /**
     * get the filter for a survey and a token, return it via array of SGQA
     * @param int $surveyId
     * @param string $token
     * @param boolean return as sgqa
     * @return array|null
     */
    private function getPartialTokenFillFilterFor($surveyId, $token, $asSGQA = false)
    {
        $oTokenFilter = \partialTokenSurveyFill\models\TokenFilter::model()->findByPk(array('sid' => $surveyId, 'token' => $token));
        if (is_null($oTokenFilter)) {
            return null;
        }
        if ($asSGQA) {
            return $oTokenFilter->getSgqaFilters();
        }
        return $oTokenFilter->getFilter();
    }

    /**
     * Return the HTML needed for managing new user to fill this survey
     * @param integer $surveyId
     * @param string $token
     * @return null|string
     */
    private function getManagementTokenSurveyFill($surveyId, $token)
    {
        /* Start to search a related tokenFilter model : if not : disable usage */
        if (\partialTokenSurveyFill\models\TokenFilter::model()->findByPk(array('sid' => $surveyId, 'token' => $token))) {
            return;
        }
        if (!$this->checkPartialTokenTokenHaveRight($surveyId, $token)) {
            return;
        }
        $oToken = Token::model($surveyId)->findByToken($token);
        $srid = isset($_SESSION['survey_' . $surveyId]['srid']) ? $_SESSION['survey_' . $surveyId]['srid'] : null;
        if (empty($srid)) {
            return;
        }
        $language = Yii::app()->getLanguage();
        $aGroups = \partialTokenSurveyFill\Utilities::getAllGroupsWithQuestions($surveyId, $language);
        $emailToken = new \partialTokenSurveyFill\helpers\EmailToken($surveyId, $this->get('emailTemplateInvite', 'Survey', $surveyId, "invite"));
        if (!$this->get('emailShowFirstname', 'Survey', $surveyId, 0)) {
            $emailToken->shownAttributes = array_diff($emailToken->shownAttributes, array('firstname'));
        }
        if (!$this->get('emailShowLarstname', 'Survey', $surveyId, 1)) {
            $emailToken->shownAttributes = array_diff($emailToken->shownAttributes, array('lastname'));
        }
        $emailData = $emailToken->getFormData();
        $renderData = array(
            'aGroups' => $aGroups,
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
            'lang' => array(
                "Select the question to send to an helper" => $this->gT("Select the question to send to an helper"),
                "Email and message" => $this->gT("Email and message"),
                "Email adress" => $this->gT("Email adress"),
                "Close" => $this->gT("Close"),
                "All question in this group" => $this->gT("All question in this group"),
                "Your contact information (Last name and first name)" => $this->gT("Your contact information (Last name and first name)"),
                "Your email" => $this->gT("Your email"),
            ),
            'addUser' => array(
                'action' => Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'sid' => $surveyId,'currenttoken' => $oToken->token,'srid' => $srid,'action' => 'adduser')),
                'email' => $emailData,
                'byname' => $oToken->firstname . " " . $oToken->lastname,
                'bymail' => $oToken->email,
            ),
        );
        $htmlDialog = Yii::app()->getController()->renderPartial(get_class($this) . '.views.dialogNewSurveyFill', $renderData, 1);
        return $htmlDialog;
    }

    /**
     * Check if a token is allowed to create a partial response
     * @param int $surveyId
     * @param string $token
     * return boolean
     */
    private function checkPartialTokenTokenHaveRight($surveyId, $token)
    {
        $attribute = $this->get('tokenattribute', 'Survey', $surveyId, "");
        $oToken = Token::model($surveyId)->findByToken($token);
        if (empty($oToken)) {
            return false;
        }
        if ($attribute) {
            if (array_key_exists($attribute, $oToken->attributes)) {
                if (empty($oToken->$attribute) || trim($oToken->$attribute) == "" ||  trim($oToken->$attribute) == "0") {
                    return false;
                }
            } else {
                $this->log("The $attribute not exist in token table of survey $surveyId, test is disable", 'warning');
            }
        }
        if (!empty($this->getPartialTokenFillFilterFor($surveyId, $token))) {
            return false;
        }
        return true;
    }

    /**
     * Check if plugin is active for current survey
     * @param $survey
     * @return boolean
     */
    private function isPartialTokenFillActive($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            return false;
        }
        if (!$this->get('active', 'Survey', $surveyId, false)) {
            return false;
        }
        if (!$oSurvey->getHasTokensTable()) {
            return false;
        }
        if (!$oSurvey->getHasResponsesTable()) {
            return false;
        }
        if ($oSurvey->getIsAnonymized()) {
            return false;
        }
        return true;
    }

    /**
     * get the token attribute in listData
     * @param integer $surveyId
     * @param string $prefix
     * @return array
     */
    private function getPartialTokenFillTokensAttributeList($surveyId, $prefix = "")
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aTokens = array(
            $prefix . 'firstname' => gT("First name"),
            $prefix . 'lastname' => gT("Last name"),
            $prefix . 'token' => gT("Token"),
            $prefix . 'email' => gT("Email"),
        );
        foreach ($oSurvey->getTokenAttributes() as $attribute => $information) {
            $aTokens[$prefix . $attribute] = empty($information['description']) ? $attribute : $information['description'];
        }
        return $aTokens;
    }

    /**
     * Just a quickest and cleaner way to return json
     */
    private function returnJson($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        Yii::app()->end();
    }

    /**
     * Own translation using parent::gT
     * @see parent::gT
     */
    public function gT($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($string, $sEscapeMode, $sLanguage);
    }
}
