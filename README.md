# partialTokenSurveyFill

Allow, in not-anomymous survey with token, to allow particpant to send invitation for filling a part of survey by another token user.

## Installation

This plugin is tested with LimeSurvey 3.

**This plugin need installation of [emailToken](https://gitlab.com/SondagesPro/coreAndTools/emailSurveyToken) plugin.**

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/partialTokenSurveyFill directory `git clone https://gitlab.com/SondagesPro/SurveyAccess/partialTokenSurveyFill.git partialTokenSurveyFill`

### Via ZIP dowload
- Download <https://dl.sondages.pro/partialTokenSurveyFill.zip>
- Extract : `unzip partialTokenSurveyFill.zip`
- Move the directory to  plugins/ directory inside LimeSUrvey

## Usage

The plugin need **survey with token** and **not anonymous**. You can set settings in _simple plugin settings_ of survey.

- Activate : on/off (default to off)
- Allow all question : allow to ask help for alll questions in a survey (default to on).
- Participant allowed to ask another user help : you can choose what attribute is used to allow user to ask for help.
- Mail template to use for invitation : choose the mail template to be used
- Shown first name when adding an helper
- Shown last name when adding an helper

**The plugin need javascript to be activated when doing survey.**

### Personalized button and multiple buttons

By default , plugin add a new button after the default navigator from LimeSurvey. If you update the theme : removing this navigator or put a different id : no button are added.

You can add any button you want adding a tag with data at showtokenfillmanagement. For example
```
<button type='button' data='showtokenfillmanagement' class='btn btn-default'>Add someone to fill this survey</button>
```

Another way are adding elemnt with class name to `tokenfillmanagement-button-wrapper` , the default button are added to each of this element.

Adding a button didable the default button, adding wrapper didn't disable navigator button.

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/>
- Code repository <https://gitlab.com/SondagesPro/SurveyAccess/partialTokenSurveyFill>
- Copyright © 2018-2020 Denis Chenu <www.sondages.pro>
- Copyright © 2018-2020 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](https://gnu.org/licenses/gpl-3.0.txt) licence
